﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class adjustLIndexSel : MonoBehaviour
{

    public GameObject LIndexUp;
    public GameObject LIndexDown;
    public GameObject LIndexSelectorDot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.Get(OVRInput.Touch.PrimaryIndexTrigger))
        {
            LIndexSelectorDot.transform.position = LIndexDown.transform.position; 
        }
        else
        {
            LIndexSelectorDot.transform.position = LIndexUp.transform.position;
        }
    }
}
