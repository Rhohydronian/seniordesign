﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

[System.Serializable]
public class Question
{
    public string prompt="";
    public string tag="";
    public int attempts = 3;
}

[System.Serializable]
public class Questions
{
    public Question[] questions;
}

public class Manager : MonoBehaviour
{
    public string[] data;
    public int fade_delay = 10;
    public float timer = 0.0f;
    public float hud_timer = 0f;
    public TextAsset jsonfile;
    public int q_index = 0;
    public GameObject q_tag;
    public Text timer_text;
    public Text hud_text;
    public Color originalColor;
    public GameObject canvas;

    public static Questions test_questions;
    public static bool runtest = false;
    public static bool buildtest = false;
    public static bool freeplay = true;
    public static string target = "";
    public static int response = 0;
    
    public bool wait = false;

    void Start()
    {
        canvas = GameObject.FindGameObjectWithTag("HUD");
        Transform text_obj = canvas.transform.Find("HudText");
        hud_text = text_obj.GetComponent<Text>();
        originalColor = hud_text.color;
        test_questions = new Questions();
        fade_delay = 3;
    }

    void Update() 
    {
        if(runtest == true)
        {
            timer_text.text = update_timer();
            update_test();
        } 
        else if(buildtest == true)
        { 
            timer_text.text = "";
            if(response == 1)
            {
                StopAllCoroutines();
                hud_text.text = target;
                hud_text.color = originalColor;
                Question new_question = new Question();
                new_question.prompt = "Find the " + target;
                new_question.tag = target;
                new_question.attempts = 3;
                add_question(new_question);
                response = 0;
                timer = 0f;
            }
        }
        else
        {
            timer_text.text = "";
            if(response == 1)
            {
                StopAllCoroutines();
                hud_text.text = target;
                hud_text.color = originalColor;
                response = 0;
                timer = 0f;
            }
        }
        
        if(timer > fade_delay && hud_text.text != "")
        {
            timer = 0f;
            StopAllCoroutines();
            StartCoroutine(FadeOutRoutine());
        }
        timer+=Time.deltaTime;  
    }

    void add_question(Question new_question)
    {
        Questions tmp_list = new Questions();
        if(q_index != 0)
        {
            tmp_list.questions = new Question[q_index];
            for (int i=0; i < test_questions.questions.Length; i++)
            {
                tmp_list.questions[i] = test_questions.questions[i];
            }
        }

        q_index++;
        test_questions.questions = new Question[q_index];

        if(q_index-1 != 0)
        {
            for(int i =0; i< tmp_list.questions.Length; i++)
            {
                test_questions.questions[i] = tmp_list.questions[i];
            }
        }
        test_questions.questions[q_index-1] = new_question;
    }

    /*void Save()
    {
            int tmp = 0;
            DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath);
            FileInfo[] info = dir.GetFiles("*.json");
            foreach (FileInfo f in info)
            {
                string result = f.ToString();
                while(result.IndexOf('\\') != -1)
                {
                    result = result.Substring(result.IndexOf('\\') + 1);
                }
                Debug.Log(result);
                tmp++;
            }
            string final = JsonUtility.ToJson(test_questions);
            System.IO.File.WriteAllText(Application.persistentDataPath +"/" + "Test" + (tmp+1) + ".json", final);
            Debug.Log(Application.persistentDataPath); 
    }*/

    void Ask(Question question)
    {
        target = question.tag;
        Print_text(question.prompt);
    }





    void Print_text(string prompt)
    {
        hud_text.text = prompt;
    }




    string update_timer()
    {
        hud_timer += Time.deltaTime;
        
        int seconds = (int)(hud_timer % 60);
        int minutes = (int)(hud_timer /60) % 60;
        int hours = (int)(hud_timer / 3600) % 24;

        string timer_str = string.Format("{0:0}:{1:00}:{2:00}",hours,minutes,seconds);
        return timer_str;
        
    }


    void update_test()
    {
        if(wait == false)
        {
            if(q_index < test_questions.questions.Length)
            {
                timer = 0f;
                hud_text.text = "";
                hud_text.color = originalColor;
                Ask(test_questions.questions[q_index]);
                wait = true;
            }
            else
            {
                q_index = 0;
                hud_text.text = "End Of Test";
                runtest = false;
                freeplay = true;
                timer = 0f;
                target = "";
                response = 0;
                fade_delay = 3;
            }
        }
        
        if(response == 1)
        {
            if(test_questions.questions[q_index].attempts > 0)
            {
                test_questions.questions[q_index].attempts = test_questions.questions[q_index].attempts - 1;
                response = 0;
                timer = 0f;
            }
            else
            {
                timer = 0f;
                StartCoroutine(IndicateCorrectAnswer());
                wait = false;
                response = 0;
                q_index++;
            }
        }
        //correct response
        else if(response == 2)
        {
            wait = false;
            response = 0;
            q_index++;
        } 
        else
        {
            wait = true;
            response = 0;
        }
    }

    public void LoadTest(string test)
    {
        test_questions = new Questions();
        test = test + ".json";
        StreamReader reader = new StreamReader(Application.persistentDataPath + "/" + test);
        string jsondata = reader.ReadToEnd();
        test_questions = JsonUtility.FromJson<Questions>(jsondata);
        q_index = 0;
        fade_delay = 7;
    }



    private IEnumerator FadeOutRoutine()
    { 
        hud_text.color = originalColor;
        for (float t = 0.01f; t < 2; t += Time.deltaTime)
        {
            hud_text.color = Color.Lerp(originalColor, Color.clear, Mathf.Min(1, t/3));
            yield return null;
        }
        hud_text.text = "";
    }


    private IEnumerator IndicateCorrectAnswer()
    { 
        GameObject target_obj = GameObject.FindGameObjectWithTag(target);
        Renderer rend = target_obj.GetComponentInChildren<Renderer>();
        Color original = rend.material.color;
        for(int i = 0; i<3; i++)
        {
            for (float t = 0.01f; t < .5f; t += Time.deltaTime)
            {
                rend.material.color = Color.Lerp(original, Color.green, Mathf.Min(1, t/.5f));
                yield return null;
            }
            for (float t = 0.01f; t < .5f; t += Time.deltaTime)
            {
                rend.material.color = Color.Lerp(Color.green, original, t/.5f);
                yield return null;
            }
        }
    }
}
//thoughts:
//make sure to set in interactables that the response variable in this script is set to true when the user hits it.