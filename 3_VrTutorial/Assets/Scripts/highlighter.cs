﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class highlighter : MonoBehaviour
{

    //stating that this box IS the one to be selected

    public bool leftIndexSelected = false;
    public bool rightIndexSelected = false;


    //true if this is the correct object


    enum HandInEnum { zero, one, two };
    HandInEnum indexesInState = HandInEnum.zero;

    enum ChoiceEnum { RGHightOn, RGHighOff };
    ChoiceEnum rgClr = ChoiceEnum.RGHighOff;

    public int redCol;
    public int greenCol;
    public int blueCol;

    Color32 origColor; //hold previous color of object being selected
    private bool l_axisInUse = false;
    private bool r_axisInUse = false;

    AudioSource audioSource;
    //hold the audio source
    [SerializeField] AudioClip clickingSound;
    [SerializeField] AudioClip wrongSound;
    [SerializeField] AudioClip correctSound;
    [SerializeField] AudioClip defaultSound;
    [SerializeField] AudioClip loudAmpCickVib;



    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {


        
        //entering highlight space for left hand
        if (other.gameObject.tag == "leftIndex")
        {

            //play click sound
            audioSource.PlayOneShot(clickingSound);
            vibrationManager.singleton.TriggerVibration(loudAmpCickVib, OVRInput.Controller.LTouch);
            // OVRInput.SetControllerVibration( 0.1f,  0.1f, OVRInput.Controller.LTouch);

            if (rgClr == ChoiceEnum.RGHighOff) // dont mess with highlight of this object while it is red or green
            {

                if (indexesInState == HandInEnum.zero) //if first hand in trigger space then stores original color
                {
                    origColor = this.GetComponentInChildren<Renderer>().material.color;
                }

                this.GetComponentInChildren<Renderer>().material.color = new Color32(100, 100, 245, 255);
            }

            indexesInState++;
            leftIndexSelected = true;
        }


        //entering trigger highlight space for right
        if (other.gameObject.tag == "rightIndex")
        {
            audioSource.PlayOneShot(clickingSound);
            vibrationManager.singleton.TriggerVibration(loudAmpCickVib, OVRInput.Controller.RTouch);

            if (rgClr == ChoiceEnum.RGHighOff) // dont mess with highlight of this object while it is red or green
            {
                if (indexesInState == HandInEnum.zero)
                {
                    origColor = this.GetComponentInChildren<Renderer>().material.color;
                }
                    this.GetComponentInChildren<Renderer>().material.color = new Color32(100, 100, 245, 255);

            }

            indexesInState++;
            rightIndexSelected = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "leftIndex")
        {
            // if it was the last hand to leave trigger space and its is not in red or green color
            if ((indexesInState == HandInEnum.one) && (rgClr == ChoiceEnum.RGHighOff))
                this.GetComponentInChildren<Renderer>().material.color = origColor;

            leftIndexSelected = false;
            indexesInState--;
            
        }
        if (other.gameObject.tag == "rightIndex")
        {
            // if it was the last hand to leave trigger space and its is not in red or green color
            if ((indexesInState == HandInEnum.one) && (rgClr == ChoiceEnum.RGHighOff))
                this.GetComponentInChildren<Renderer>().material.color = origColor;

            rightIndexSelected = false;
            indexesInState--;

        }

    }


    // Update is called once per frame
    void Update()
    {
        //this is the highlighting for the right hand 
        if ((OVRInput.Get(OVRInput.RawAxis1D.RIndexTrigger) > 0.9f) && (rightIndexSelected == true))
        {

            //the if here is so trigger axis on controller won't be read again until player lets go
            if (r_axisInUse == false)
            {

                if (Manager.runtest == true)
                {


                    //is this the correct switch
                    if (gameObject.CompareTag(Manager.target)) // if (correctSelection == true) //if correct selection with right hand than turn green and correct sound
                    {
                        this.GetComponentInChildren<Renderer>().material.color = new Color32(0, 255, 0, 255);
                        //play sound here for correct selection
                        audioSource.PlayOneShot(correctSound);
                        //vibration for correct choice
                        vibrationManager.singleton.TriggerVibration(correctSound, OVRInput.Controller.RTouch);

                        Manager.response = 2;

                    }
                    else
                    {
                        this.GetComponentInChildren<Renderer>().material.color = new Color32(255, 0, 0, 255);
                        Manager.response = 1;
                        audioSource.PlayOneShot(wrongSound);
                        vibrationManager.singleton.TriggerVibration(wrongSound, OVRInput.Controller.RTouch);
                    }

                    rgClr = ChoiceEnum.RGHightOn;

                    //waits revert right or wrong color back  by 2 seconds
                    Invoke("RevertRWColor", 2f);
                }
                else if(Manager.buildtest == true || Manager.freeplay == true)
                {
                    //plays audio and vibration for free play and 
                    audioSource.PlayOneShot(defaultSound);
                    vibrationManager.singleton.TriggerVibration(loudAmpCickVib, OVRInput.Controller.RTouch);


                    if (gameObject.tag != "")
                    {
                        Manager.target = gameObject.tag;
                        Manager.response = 1;
                    }
                }
                r_axisInUse = true;
            }

        }
        if ((OVRInput.Get(OVRInput.RawAxis1D.RIndexTrigger) == 0f))
            r_axisInUse = false;





        //this is the highlighting for the left hand 
        if ((OVRInput.Get(OVRInput.RawAxis1D.LIndexTrigger) > 0.9f) && (leftIndexSelected == true))
        {
            // play vibration hear for right controller

            if (l_axisInUse == false)
            {
                if (Manager.runtest == true)
                {
                    //is this the correct switch
                    if (gameObject.CompareTag(Manager.target)) // if (correctSelection == true) //if correct selection with right hand than turn green and correct sound
                    {
                        this.GetComponentInChildren<Renderer>().material.color = new Color32(0, 255, 0, 255);
                        //play sound here for correct selection
                        audioSource.PlayOneShot(correctSound);

                        vibrationManager.singleton.TriggerVibration(correctSound, OVRInput.Controller.LTouch);

                        Manager.response = 2;

                    }
                    else
                    {
                        this.GetComponentInChildren<Renderer>().material.color = new Color32(255, 0, 0, 255);
                        Manager.response = 1;
                        audioSource.PlayOneShot(wrongSound); //plays wrong sound
                        vibrationManager.singleton.TriggerVibration(correctSound, OVRInput.Controller.LTouch); //vibration for wrong sound

                    }

                    rgClr = ChoiceEnum.RGHightOn;

                    //waits revert right or wrong color back  by 2 seconds
                    Invoke("RevertRWColor", 2f);
                }
                else if(Manager.buildtest == true || Manager.freeplay == true)
                {

                    audioSource.PlayOneShot(defaultSound);
                    vibrationManager.singleton.TriggerVibration(loudAmpCickVib, OVRInput.Controller.LTouch);
                    if (gameObject.tag != "")
                    {
                        Manager.target = gameObject.tag;
                        Manager.response = 1;
                    }
                }
                l_axisInUse = true;
            }

        }
        if ((OVRInput.Get(OVRInput.RawAxis1D.LIndexTrigger) == 0f))
            l_axisInUse = false;




    }



        //this function reverts the color back to what it was before it was selected from right or wrong
        public void RevertRWColor()
        {

            if (rightIndexSelected == true || leftIndexSelected == true)
                this.GetComponentInChildren<Renderer>().material.color = new Color32(245, 245, 245, 255);
            else
                this.GetComponentInChildren<Renderer>().material.color = origColor;

            rgClr = ChoiceEnum.RGHighOff;
        }
    
}
