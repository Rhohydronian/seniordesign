﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    public GameObject pauseMenu;
    public bool isPaused;

    public  GameObject ModeMenu;
    public  GameObject SaveMenu;
    public  GameObject ChooseTestMenu;
    public Dropdown test_dropdown;
    public Manager manager_script;


    // Start is called before the first frame update
    void Start()
    {
        manager_script = GameObject.FindGameObjectWithTag("MANAGER").GetComponent<Manager>();
        pauseMenu.SetActive(false);
        isPaused = false;
        ModeMenu.SetActive(true);
        SaveMenu.SetActive(false);
        ChooseTestMenu.SetActive(false);
        PopulateList();
       
    }

    // Update is called once per frame
    void Update()
    {

        //when user presses start to bring up menu in game
       if(OVRInput.GetDown(OVRInput.Button.Start))
        {
            print("its was pressed");
            if (isPaused)
            {
                isPaused = false;
                pauseMenu.SetActive(false);
            }else
            {
                isPaused = true;
                pauseMenu.SetActive(true);
            }
        }


    }

    public void ResumeGame()
    {
           isPaused = false;
            pauseMenu.SetActive(false);
    }

    public void ReturnToMainScene()
    {
       // SceneManager.LoadScene(mainMenuScene);
    }

    public void LoadTestMechanicScene()
    {
        SceneManager.LoadScene(1);
    }

   
    public void LoadStartMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }


    //button functions. These are all the functions that will be attached to the buttons. 
    public void freeModeButton()
    {
        freeMode();
        ResumeGame();
    }

    public void designTestButton()
    {
        createTestMode();
        SaveMenu.SetActive(true);
        ModeMenu.SetActive(false);
    }

    //display menu for choosing a test
    public void TakeTestButton()
    {
        ChooseTestMenu.SetActive(true);
        ModeMenu.SetActive(false);
    }

    public void SelectTest()
    {
        int current_index = test_dropdown.value;
        string test = test_dropdown.options[current_index].text;
        testMode();
        manager_script.LoadTest(test);
    }

    public void PopulateList()
    {
        List<string> test_list = new List<string>();
        DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath);
        FileInfo[] info = dir.GetFiles("*.json");
        test_list.Add("-----");
        foreach (FileInfo f in info)
        {
            string result = f.ToString();
            while(result.IndexOf('\\') != -1)
            {
                result = result.Substring(result.IndexOf('\\') + 1);
            }
            int index = result.IndexOf('.');
            result = result.Substring(0, index);
            test_list.Add(result);
        }
        test_dropdown.ClearOptions();
        test_dropdown.AddOptions(test_list);
    }

    
    public void savetestButton()
    {
            int tmp = 0;
            DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath);
            FileInfo[] info = dir.GetFiles("*.json");
            foreach (FileInfo f in info)
            {
                Debug.Log(f.ToString());
                tmp++;
            }
            string final = JsonUtility.ToJson(Manager.test_questions);
            System.IO.File.WriteAllText(Application.persistentDataPath +"/" + "Test" + (tmp+1) + ".json", final);
            PopulateList();
        // make game free mode
        SaveMenu.SetActive(false);
        ModeMenu.SetActive(true); 

    }

    public void goBackButton()
    {
        ModeMenu.SetActive(true);
        ChooseTestMenu.SetActive(false);
        freeMode();
        PopulateList();
    }

    //These 3 functions controll what mode we are in 
    public void testMode()
    {

        Manager.runtest = true;
        Manager.buildtest = false;
        Manager.freeplay = false;
    }

    public void createTestMode()
    {
        Manager.runtest = false;
        Manager.buildtest = true;
        Manager.freeplay = false;
    }

    public void freeMode()
    {
        Manager.runtest = false;
        Manager.buildtest = false;
        Manager.freeplay = true;
    }
}
