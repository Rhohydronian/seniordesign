﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDetector : MonoBehaviour
{

    [SerializeField] GameObject gameobject;
    [SerializeField] Collider triggerSpace;

    private void OnTriggerEnter(Collider otherObjectTouched)
    {

        GetComponentInParent<Animator>().SetTrigger("Move Button");
        print("Button Pressed");
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
