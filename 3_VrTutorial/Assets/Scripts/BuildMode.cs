﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
public class BuildMode : MonoBehaviour
{
    public int q_index = 0;
    public Questions test_questions = new Questions();
    public Question new_question = new Question();
    public TMP_Dropdown dropdown;
    public TMP_InputField promptInput;
    public TMP_InputField test_name;
    public TMP_InputField attempts;
    
    void Start() 
    {
        dropdown = GameObject.FindGameObjectWithTag("DROPDOWN").GetComponent<TMP_Dropdown>();
        promptInput = GameObject.FindGameObjectWithTag("PROMPT").GetComponent<TMP_InputField>();
        test_name = GameObject.FindGameObjectWithTag("NAME").GetComponent<TMP_InputField>();
        attempts = GameObject.FindGameObjectWithTag("Attempts").GetComponent<TMP_InputField>();
    }
    public void HandleTag()
    {
       new_question.tag = dropdown.options[dropdown.value].text;
       Debug.Log(new_question.tag);
    }

    public void HandlePrompt()
    {
        new_question.prompt = promptInput.text;
        Debug.Log(new_question.prompt);
    }

    public void HandleName()
    {
        new_question.prompt = promptInput.text;
        Debug.Log(new_question.prompt);
    }

    public void HandleAttempts()
    {
        new_question.attempts = Int32.Parse(attempts.text);
        Debug.Log(new_question.attempts); 
    }

    void add_question()
    {
        q_index++;
        Questions tmp_list = new Questions();
        tmp_list = test_questions;
        test_questions.questions = new Question[q_index];
        for(int i =0; i< tmp_list.questions.Length; i++)
        {
            test_questions.questions[i] = tmp_list.questions[i];
        }
        test_questions.questions[q_index-1] = new_question;
    }

}
