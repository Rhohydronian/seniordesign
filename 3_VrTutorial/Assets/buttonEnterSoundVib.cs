﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class buttonEnterSoundVib : MonoBehaviour, IPointerEnterHandler
{

    AudioSource audioSource;
    //hold the audio source
    [SerializeField] AudioClip clickingSound;
    [SerializeField] AudioClip loudAmpCickVib;



    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }


    public void OnPointerEnter(PointerEventData eventdata)
    {
        audioSource.PlayOneShot(clickingSound);
        vibrationManager.singleton.TriggerVibration(loudAmpCickVib, OVRInput.Controller.RTouch);
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
