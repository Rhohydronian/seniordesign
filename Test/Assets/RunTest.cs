﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Question
{
    public string prompt;
    public string tag;
}

[System.Serializable]
public class Questions
{
    public Question[] questions;
}

public class RunTest : MonoBehaviour
{
    public float timer = 0.0f;
    public TextAsset jsonfile;
    public Questions test_questions;
    public int q_index = 0;
    public GameObject q_tag;
    public Text hud_text;
    public GameObject canvas;
    public bool test = false;

    public static bool runtest = true;
    public static string target = "";
    public static int response = 0;
    
    public bool wait = false;

    void Start()
    {
        canvas = GameObject.FindGameObjectWithTag("HUD");
        Transform text_obj = canvas.transform.Find("HudText");
        hud_text = text_obj.GetComponent<Text>();

        test_questions = JsonUtility.FromJson<Questions>(jsonfile.text);
        
        foreach (Question question in test_questions.questions)
        {
            Debug.Log("-----" + question.prompt + "----" + question.tag);
        }
    }

    void Update() 
    {
        if(runtest == true && wait == false)
        {
            if(q_index < test_questions.questions.Length)
            {
                Ask(test_questions.questions[q_index]);
                wait = true;
                q_index++;
            }
            else
            {
                q_index = 0;
                Print_text("End Of Test");
                runtest = false;
            }
        }
        //incorrect response
        if(response == 1)
        {
            //LEFT OFF NEED TO GET JOHNS CODE AND BEGIN TESTING********************
            //insert attempts code here
            wait = false;
            response = 0;
        }
        //correct response
        else if(response == 2)
        {
            wait = false;
            response = 0;
        } 
    }



    void Ask(Question question)
    {
        target = question.tag;
        Print_text(question.prompt);
    }


    void Print_text(string prompt)
    {
        hud_text.text = prompt;
    }
}
//thoughts:
//make sure to set in interactables that the response variable in this script is set to true when the user hits it.