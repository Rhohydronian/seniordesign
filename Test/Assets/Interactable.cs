﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public int triggered = 0;
    public bool correct = false;
    public int activate_green_corona = 0;
    public int activate_red_corona = 0;
    public int timer = 5;
    public int test = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
    //does this only run on when the object is in.
    private void OnTriggerEnter(Collider other) 
    {
        if(RunTest.runtest == true)
        {                               
            if(gameObject.CompareTag(RunTest.target))         //add the following when i have the parts "&& user.select"
                {
                    activate_green_corona = 1;
                    RunTest.response = 2;
                }
            else                           // change to else if and add "&& user.select"
                {
                    activate_red_corona = 1;
                    RunTest.response = 1;
                }
        }

    }
}
